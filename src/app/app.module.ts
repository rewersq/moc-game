import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {MdfComponent} from './mdf/mdf.component';
import {PortComponent} from './port/port.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';
import {NetworkComponent} from './network/network.component';
import {ConnectionComponent} from './connection/connection.component';
import {DraggableModule} from './draggable/draggable.module';


@NgModule({
  declarations: [
    AppComponent,
    MdfComponent,
    PortComponent,
    NetworkComponent,
    ConnectionComponent

  ],
  imports: [
    BrowserModule,
    DraggableModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    )
  ],
  // schemas: [
  //   CUSTOM_ELEMENTS_SCHEMA,
  // ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
