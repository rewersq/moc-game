import {Component, OnInit} from '@angular/core';
import {MDF} from '../mdf/mdf';
import {MdfService} from '../mdf.service';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit {


  mdfs: MDF[];

  constructor(private mdfService: MdfService) {
  }

  getMDFs(): void {
    this.mdfService.getMDFs().subscribe(mdfs => this.mdfs = mdfs);
  }

  ngOnInit() {
    this.getMDFs();
    console.log(this.mdfs);

  }

}
