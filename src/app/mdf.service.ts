import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MDF} from './mdf/mdf';

@Injectable({
  providedIn: 'root'
})
export class MdfService {
  private url = 'api/mdfs';

  constructor(private http: HttpClient) {
  }

  getMDFs(): Observable<MDF[]> {
    return this.http.get<MDF[]>(this.url);
  }
}
