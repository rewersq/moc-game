import {Component} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  onDragStart(): void {
    console.log('got drag start');
  }

  onDragEnd(): void {
    console.log('got drag end');
  }

  onDragMove(event: PointerEvent): void {
    console.log(`got drag move ${Math.round(event.clientY)} : ${Math.round(event.clientX)}`);
  }
}
