import {Injectable} from '@angular/core';
import {Port} from './port/port';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PortService {
  private url = 'api/ports';

  constructor(private http: HttpClient) {
  }

  getObjectPorts(id: number, type: string): Observable<Port[]> {
    return this.http.get<Port[]>(`${this.url}?object=${type}&object_id=${id}`);
  }

  getPort(number: number): Observable<Port> {
    return this.http.get<Port>(`${this.url}/number=${number}`);
  }
}
