import {Component, OnInit} from '@angular/core';
import {Connection} from './connection';
import {ConnectionService} from '../connection.service';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent implements OnInit {
  private connections: Connection[];

  constructor(private connectionService: ConnectionService) {
  }

  getConnections(): void {
    this.connectionService.getConnections().subscribe(connections => this.connections = connections);
  }

  ngOnInit() {
    this.getConnections();
  }

}
