import {Component, Input, OnInit} from '@angular/core';
import {Port} from './port';

@Component({
  selector: 'app-port',
  templateUrl: './port.component.html',
  styleUrls: ['./port.component.css']
})
export class PortComponent implements OnInit {
  @Input() port: Port;

  constructor() {
  }

  ngOnInit() {
  }

}
