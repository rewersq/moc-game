import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const mdfs = [

      {id: 1, type: 'horizontal', destination: 'tdm'},
      {id: 2, type: 'vertical'},
      {id: 3, type: 'horizontal', destination: 'ims'}
    ];
    const ports = [

      {number: 1, object: 'MDF', object_id: 1},
      {number: 2, object: 'MDF', object_id: 1},
      {number: 3, object: 'MDF', object_id: 1},
      {number: 4, object: 'MDF', object_id: 2},
      {number: 5, object: 'MDF', object_id: 2},
      {number: 6, object: 'MDF', object_id: 2},
      {number: 7, object: 'MDF', object_id: 3},
      {number: 8, object: 'MDF', object_id: 3},

    ];

    const connections = [
      {end: 4, start: 1},
      {start: 2, end: 5},
      {start: 3, end: 7},
      {start: 6, end: 8}

    ];
    return {mdfs, ports, connections};
  }
}
