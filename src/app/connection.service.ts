import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Connection} from './connection/connection';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private url = 'api/connections';

  constructor(private http: HttpClient) {
  }

  getConnections(): Observable<Connection[]> {
    return this.http.get<Connection[]>(this.url);
  }
}
