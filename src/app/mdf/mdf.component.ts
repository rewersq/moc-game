import {Component, Input, OnInit} from '@angular/core';
import {MDF} from './mdf';
import {PortService} from '../port.service';
import {Port} from '../port/port';

@Component({
  selector: 'app-mdf',
  templateUrl: './mdf.component.html',
  styleUrls: ['./mdf.component.css']
})
export class MdfComponent implements OnInit {

  @Input() mdf: MDF;
  private ports: Port[];

  constructor(private portService: PortService) {
  }

  getPorts(): void {
    this.portService.getObjectPorts(this.mdf.id, 'MDF').subscribe(ports => this.ports = ports);
  }

  ngOnInit() {
    this.getPorts();

  }

}
